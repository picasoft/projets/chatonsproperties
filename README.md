# ChatonsProperties

Ce dépôt est une première étape pour stocker les fichiers permettant d'alimenter ChatonsInfos.

Plus d'infos sur ChatonsInfos ici : https://framagit.org/chatons/chatonsinfos

Dans un premier temps, ces fichiers sont générés "à la main" et mis à jour à la main.

Certains des fichiers contenant des références à des métriques (servant probablement à faire des graphes), il est envisageable de les mettre à jour automatiquement.

Il pourra être également intéressant de faire une CI pour téléverser les fichier sur le SFTP de Picasoft quand ils sont mis à jour ici.

On choisit une bonne fois pour toutes l'URL de base de ces fichiers : https://uploads.picasoft.net/.well-known/chatonsinfos
C'est cette URL qui sera référencée ici : https://framagit.org/chatons/chatonsinfos/-/blob/master/StatoolInfos/chatons.properties

Le fait de choisir `uploads` permet de téléverser les fichiers via le [SFTP](https://wiki.picasoft.net/doku.php?id=technique:adminserv:sftp).

## TODO

* Expliquer le fonctionnement des services (section sub du fichier principal dans `general`), mais ce sera probablement amené à évoluer
* Uploads CI
* Ajouter une doc sur le Wiki, dans la partie des tâches routinières (à construire) pour ne pas oublier ce dépôt
